'use strict';
require('dotenv').config();
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const winston = require('winston');
const PropertiesReader = require('properties-reader');
const properties = PropertiesReader(`${__dirname.split('\\src')[0]}/src/bin/common.properties`);

//Import controllers which hold the CRUD methods for each model
const clientesRouter = require('./src/routes/clientesRouter');
//Instantiate Express
const app = express();
const root = properties.get('routes.api.index');
const version = properties.get('routes.api.version');
const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log`
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
    ]
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'development') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple()
    }));
}
app.use(cors());
//Set up body-parser with JSON
app.use(bodyParser.json());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

/**
 * Se definen todos los enrutadores que utilizara el API
 * */
app.use(version, clientesRouter);


//===========================================
// End of Route Handlers
//===========================================

// Catch 404 errors and forward to error handler. This is called if no match is found in the preceding route functions.
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//Tell node to listen for the App on port 3000:
// listen on port 3000
app.listen(process.env.PORT || 5000, function () {
    console.log('Express app listening on port 5000');
});
