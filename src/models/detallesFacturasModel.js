/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let detallesModel =  sequelize.define('detallesModel', {
    Id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'dfa_id'
    },
    facId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'fac_id'
    },
    Descripcion: {
      type: DataTypes.STRING(100),
      allowNull: false,
      field: 'dfa_descripcion'
    },
    Unidades: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'dfa_unidades'
    },
    MontoUnitario: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'dfa_monto_unitario'
    },
    MontoItem: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'dfa_monto_item'
    }
  }, {
    tableName: 'dfa_detalle_facturas',
    timestamps: false
  });

  detallesModel.associate = function(models) {
    detallesModel.belongsTo(models.facturasModel,{
      as: 'factura',
      foreignKey: 'facId'
    });
  };

  return detallesModel;
};
