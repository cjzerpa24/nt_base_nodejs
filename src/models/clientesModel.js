/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let clientesModel = sequelize.define('clientesModel', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'cli_id'
    },
    codigo: {
      type: DataTypes.STRING(30),
      allowNull: false,
      field: 'cli_codigo'
    },
    nombre: {
      type: DataTypes.STRING(60),
      allowNull: false,
      field: 'cli_nombre'
    },
    direccion: {
      type: DataTypes.STRING(100),
      allowNull: false,
      field: 'cli_direccion'
    },
    correo: {
      type: DataTypes.STRING(30),
      allowNull: true,
      field: 'cli_correo'
    },
    telefono: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: 'cli_telefono'
    },
    status: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1',
      field: 'cli_status'
    }
  }, {
    tableName: 'cli_clientes',
    timestamps: false
  });

  clientesModel.associate = function (models) {
    clientesModel.hasMany(models.facturasModel, {
      as: 'facturas',
      foreignKey: 'id'
    });
  };

  return clientesModel;
};
