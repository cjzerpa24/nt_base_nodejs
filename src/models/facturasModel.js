/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let facturasModel = sequelize.define('facturasModel', {
    Id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'fac_id'
    },
    cliId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'cli_id'
    },
    FechaCreacion: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      field: 'fac_fecha_creacion'
    },
    CodigoFactura: {
      type: DataTypes.STRING(30),
      allowNull: false,
      field: 'fac_codigo_factura'
    },
    BaseImponible: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'fac_base_imponible'
    },
    Impuesto: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'fac_impuesto'
    },
    PorcentajeImpuesto: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'fac_porcentaje_impuesto'
    },
    MontoTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'fac_monto_total'
    }
  }, {
    tableName: 'fac_facturas',
    timestamps: false
  });

  facturasModel.associate = function(models) {
    facturasModel.belongsTo(models.clientesModel, {
      as: 'clientes',
      foreignKey: 'cliId'
    });

    facturasModel.hasMany(models.detallesModel, {
      as: 'detalles',
      foreignKey: 'Id'
    });
  };

  return facturasModel;
};
