const models = require('../models');
const Sequelize = require('sequelize');
const HttpStatus = require('http-status-codes');
const PropertiesReader = require('properties-reader');
const properties = PropertiesReader(`${__dirname.split('\\src')[0]}/src/bin/common.properties`);
let message;
let type;
const Op = Sequelize.Op;

module.exports = {
    getAllClientes(req,res,next) {
        models.clientesModel.findAll()
        .then((clientes) => {
            if(clientes.length > 0) {
                type="success";
                res.status(HttpStatus.OK).json({message,clientes,type});
            } else {
                message=properties.get('message.cli.res.notData');
                type="Not Data";
                res.status(HttpStatus.OK).json({clientes,type});
            }
        },(err) => {
            console.dir(err);
            message = properties.get('message.res.errorInternalServer');
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({message});
            next(err);
        });
    },
    getClientesByCodigo(req,res,next) {
        models.clientesModel.findAll({
            where:{
                codigo: {
                    [Op.like]: '%'+req.params.codigo+'%'
                }
            }
        })
        .then((clientes) => {
            if(clientes.length > 0) {
                type="success";
                res.status(HttpStatus.OK).json({message,clientes,type});
            } else {
                message=properties.get('message.cli.res.notData');
                type="Not Data";
                res.status(HttpStatus.OK).json({clientes,type});
            }
        }, (err) => {
            console.dir(err);
            message = properties.get('message.res.errorInternalServer');
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({message});
            next(err);
        });
    },
    getClientesByNombre(req,res,next) {
        models.clientesModel.findAll({
            where:{
                nombre: {
                    [Op.like]: '%'+req.params.nombre+'%'
                }
            }
        })
            .then((clientes) => {
                if(clientes.length > 0) {
                    type="success";
                    res.status(HttpStatus.OK).json({message,clientes,type});
                } else {
                    message=properties.get('message.cli.res.notData');
                    type="Not Data";
                    res.status(HttpStatus.OK).json({clientes,type});
                }
            }, (err) => {
                console.dir(err);
                message = properties.get('message.res.errorInternalServer');
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({message});
                next(err);
            });
    },
    getClientesByNombreyCodigo(req,res,next) {
        models.clientesModel.findAll({
            where:{
                nombre: {
                    [Op.like]: '%'+req.params.nombre+'%'
                },
                codigo: {
                    [Op.like]: '%'+req.params.codigo+'%'
                }
            }
        })
            .then((clientes) => {
                if(clientes.length > 0) {
                    type="success";
                    res.status(HttpStatus.OK).json({message,clientes,type});
                } else {
                    message=properties.get('message.cli.res.notData');
                    type="Not Data";
                    res.status(HttpStatus.OK).json({clientes,type});
                }
            }, (err) => {
                console.dir(err);
                message = properties.get('message.res.errorInternalServer');
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({message});
                next(err);
            });
    },
    addClientes(req,res,next) {
        models.clientesModel.create({
            codigo: req.body.codigo,
            nombre: req.body.nombre,
            direccion: req.body.direccion,
            correo: req.body.correo,
            telefono: req.body.telefono
        })
        .then((clientes) => {
            message = properties.get('message.cli.res.okCreated');
            type="success";
            res.status(HttpStatus.OK).json({message,clientes,type});
        }, (err) => {
            console.dir(err);
            message = properties.get('message.res.errorInternalServer');
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({message});
            next(err);
        });
    },
    updateCliente(req,res,next) {
        models.clientesModel.findOne({
            where: {
                id: req.body.id
            }
        })
        .then((clientes) => {
            if(clientes) {
                clientes.update({
                    codigo: (req.body.codigo != null ) ? req.body.codigo : clientes.codigo,
                    nombre: (req.body.nombre != null) ? req.body.nombre : clientes.nombre,
                    direccion: (req.body.direccion != null) ? req.body.direccion : clientes.direccion,
                    correo: (req.body.correo != null) ? req.body.correo : clientes.correo,
                    telefono: (req.body.telefono != null) ? req.body.telefono : clientes.telefono,
                })
                .then((clientes) => {
                    message = properties.get('message.cli.res.cliUpdated');
                    type="success";
                    res.status(HttpStatus.OK).json({message,clientes,type});
                }, (err) => {
                    console.dir(err);
                    message = properties.get('message.res.errorInternalServer');
                    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({message});
                    next(err);
                });
            }
        }, (err) => {
            message = properties.get('message.cli.res.notDataToUpdate');
            res.status(HttpStatus.NOT_FOUND).json({message});
            next(err);
        });
    },
    updateStatusClientes(req,res,next) {
        let cliId = req.body.Id;
        if(!Array.isArray(cliId)) cliId = new Array(cliId);
        let cont =0;
        for(var i = 0; i < cliId.length; i++) {
            models.clientesModel.findOne({
                where: {
                    cli_id: cliId[i]
                }
            })
            .then((cliente) => {
                if(cliente) {
                    cliente.update({
                        cli_status: 2
                    })
                    .then((rowsAffected) => {
                        if(rowsAffected > 0){
                            cont++;
                        }
                    }, (err) => {
                        console.dir(err);
                        message = properties.get('message.res.errorInternalServer');
                        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({message});
                        next(err);
                    });
                }
            }, (err) => {
                console.dir(err);
                message = properties.get('message.cli.notData');
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({message});
                next(err);
            });
        }
        if(cont > 0 ){
            message="Se actualizaron "+parseInt(cont)+ " correctamente";
            type="success";
            res.status(HttpStatus.OK).json({message,type});
        }
    }
};
