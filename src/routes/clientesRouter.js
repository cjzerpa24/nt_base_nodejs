const express = require('express');
const PropertiesReader = require('properties-reader');
const properties = PropertiesReader(`${__dirname.split('\\src')[0]}/src/bin/common.properties`);
const clientesController = require('../controllers/clientesController');
const clientesRouter = express.Router();

const uriClientes = properties.get('routes.api.clientes');
const uriClientesCodigo = properties.get('routes.api.clientes.codigo');
const uriClientesNombre = properties.get('routes.api.clientes.nombre');
const uriClientesStatus = properties.get('routes.api.clientes.status');
const uriClientesCodigoNombre = properties.get('routes.api.clientes.codnomb');
/**
 * CRU para clientes
 * */
clientesRouter.route(uriClientes)
    .get(clientesController.getAllClientes)
    .post(clientesController.addClientes)
    .put(clientesController.updateCliente);

/**
 * Cambio estatus clientes
 * */
clientesRouter.route(uriClientesStatus)
    .put(clientesController.updateStatusClientes);

/**
 * Filtro para Cliente
 * */
clientesRouter.route(uriClientesCodigo)
    .get(clientesController.getClientesByCodigo);

clientesRouter.route(uriClientesNombre)
    .get(clientesController.getClientesByNombre);

clientesRouter.route(uriClientesCodigoNombre)
    .get(clientesController.getClientesByNombreyCodigo);


module.exports = clientesRouter;
